# goget-redirect

Quick and dirty app to handle redirects for our infrastructure.

It will resolve `go get` requests to web, api and this package and redirect all other requests to `git.kolaente.de/vikunja`.