
###################################
#Build stage
FROM golang:1.13-alpine AS build-env

ENV GO111MODULE=on
ENV GOFLAGS=-mod=vendor

#Build deps
RUN apk --no-cache add build-base git

#Setup repo
COPY . /go/src/code.vikunja.io/goget
WORKDIR /go/src/code.vikunja.io/goget
RUN go build -mod=vendor .

FROM alpine:3.7
LABEL maintainer="maintainers@vikunja.io"

EXPOSE 8080

RUN apk --no-cache add \
    bash \
    ca-certificates \
    curl \
    gettext \
    linux-pam \
    s6 \
    sqlite \
    su-exec \
    tzdata

COPY docker /
COPY --from=build-env /go/src/code.vikunja.io/goget/goget-redirect /app/goget/goget

ENV VIKUNJA_SERVICE_ROOTPATH=/app/goget/

ENTRYPOINT ["/bin/s6-svscan", "/etc/services.d"]
CMD []
